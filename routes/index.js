
/*
 * GET home page.
 */

var storage = require('node-persist');

function updateUserScore(userName, score) {
	storage.initSync();
	if (userName.length > 0) {
		var leaderboardItem = storage.getItem(userName.toUpperCase());
		if (leaderboardItem === undefined) {
			leaderboardItem = {name: userName, score: Number(0)};
		};
		if (Number(score) > Number(leaderboardItem.score)) {
			leaderboardItem.score = Number(score);
			console.log('user: ' + leaderboardItem.name + ' updated. new score = ' + leaderboardItem.score);
			storage.setItem(userName.toUpperCase(), leaderboardItem);
		};
	};
};

exports.index = function(req, res) {
	storage.initSync();
	storage.values(function(leaderboardItems) {
		var sortedLeaderboardItems = leaderboardItems.sort(function(item1, item2) { return Number(item1.score) < Number(item2.score)});
    	console.log(sortedLeaderboardItems);
    	res.render('index', { title: 'Top Scores',
  							leaderboard: sortedLeaderboardItems});
	});
};

exports.update = function(req, res) {
	var userName = req.query.name;
	var score = req.query.score;
	updateUserScore(userName, score);
	res.send('user: ' + userName + ' updated. new score = ' + score);
};

exports.destroy = function(req, res) {
	var userName = req.query.name;
	storage.initSync();
	storage.removeItem(userName.toUpperCase());
  	res.send('user: ' + userName + ' deleted');
};